var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM user;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(user_id, callback) {
    var query = 'SELECT * FROM user WHERE user_id = (?)';
    var queryData = [user_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE user
    var query = 'INSERT INTO user (first_name, last_name, email) VALUES (?)';

    var queryData = [params.first_name, params.last_name, params.email];

    connection.query(query,[queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(user_id, callback) {
    var query = 'DELETE FROM user WHERE user_id = ?';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE user SET email = ?, first_name = ?, last_name = ? WHERE user_id = ?';
    var queryData = [params.email, params.first_name, params.last_name, params.user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(user_id, callback) {
    var query = 'SELECT * FROM user WHERE user_id = (?);';
    var queryData = [user_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};