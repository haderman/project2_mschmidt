var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM videogame;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(vg_id, callback) {
    var query = 'SELECT d.* FROM game_dev gd\n' +
        'LEFT JOIN developer d on d.dev_id = gd.dev_id\n' +
        'WHERE gd.vg_id = (?);';
    var queryData = [vg_id];

    connection.query(query, queryData, function(err, dev) {
        var query = 'SELECT p.* FROM game_pub gp\n' +
            'LEFT JOIN publisher p on p.pub_id = gp.pub_id\n' +
            'WHERE gp.vg_id = (?);';

        connection.query(query, queryData, function(err, pub) {
            var query = 'SELECT * FROM videogame WHERE vg_id = (?);';
            console.log(query);


            connection.query(query, queryData, function(err, result) {
                result[2] = pub;
                result[3] = dev;
                callback(err, result);
            });
        });
    });
};

exports.insert = function(params, callback) {

    var query = 'INSERT INTO videogame(vg_name, date_released, rating, ESRB, platform) VALUES (?)';

    var queryData = [params.vg_name, params.date_released, params.rating,  params.ESRB, params.platform];

    connection.query(query, [queryData], function(err, result) {
        var vg_id = result.insertId;
        var query = 'INSERT INTO game_dev (vg_id, dev_id) VALUES ?';
        var videogameDevData = [];
        if (params.dev_id.constructor === Array) {
            for (var i = 0; i < params.dev_id.length; i++) {
                videogameDevData.push([vg_id, params.dev_id[i]]);
            }
        }
        else {
            videogameDevData.push([vg_id, params.dev_id]);
        }
        connection.query(query, [videogameDevData], function(err, result){
            var query = 'INSERT INTO game_pub (vg_id, pub_id) VALUES ?';
            var videogamePubData = [];
            if (params.pub_id.constructor === Array) {
                for (var i = 0; i < params.pub_id.length; i++) {
                    videogamePubData.push([vg_id, params.pub_id[i]]);
                }
            }
            else {
                videogamePubData.push([vg_id, params.pub_id]);
            }
            connection.query(query, [videogamePubData], function(err, result){
                callback(err, result);
            });
        });
    });

};

exports.delete = function(vg_id, callback) {
    var query = 'DELETE FROM videogame WHERE vg_id = ?';
    var queryData = [vg_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE videogame SET vg_name = ?, date_released = ?, rating = ?, ESRB = ?, platform = ? WHERE vg_id = ?';
    var queryData = [params.vg_name, params.date_released, params.rating,  params.ESRB, params.platform, params.vg_id];

    connection.query(query, queryData, function(err, result) {
        var query = 'DELETE FROM game_dev WHERE vg_id = ?';
        connection.query(query, params.vg_id, function(err, result) {
            var query = 'DELETE FROM game_pub WHERE vg_id = ?';
            connection.query(query, params.vg_id, function(err, result) {
                var query = 'INSERT INTO game_dev (vg_id, dev_id) VALUES ?';
                var videogameDevData = [];
                if (params.dev_id.constructor === Array) {
                    for (var i = 0; i < params.dev_id.length; i++) {
                        videogameDevData.push([params.vg_id, params.dev_id[i]]);
                    }
                }
                else {
                    videogameDevData.push([params.vg_id, params.dev_id]);
                }
                connection.query(query, [videogameDevData], function(err, result){
                    var query = 'INSERT INTO game_pub (vg_id, pub_id) VALUES ?';
                    var videogamePubData = [];
                    if (params.pub_id.constructor === Array) {
                        for (var i = 0; i < params.pub_id.length; i++) {
                            videogamePubData.push([params.vg_id, params.pub_id[i]]);
                        }
                    }
                    else {
                        videogamePubData.push([params.vg_id, params.pub_id]);
                    }
                    connection.query(query, [videogamePubData], function(err, result){
                        callback(err, result);
                    });
                });
            });
        });
    });
};

exports.edit = function(vg_id, callback) {
    var query = 'SELECT v.*, d.*, p.* FROM videogame v\n' +
        'LEFT JOIN game_dev gd on gd.vg_id = v.vg_id\n' +
        'LEFT JOIN developer d on d.dev_id = gd.dev_id\n' +
        'LEFT JOIN game_pub gp on gp.vg_id = v.vg_id\n' +
        'LEFT JOIN publisher p on p.pub_id = gp.pub_id\n' +
        'WHERE v.vg_id = (?);';
    var queryData = [vg_id];
    console.log(query);

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};