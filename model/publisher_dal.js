var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM publisher;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(publisher_id, callback) {
    var query = 'SELECT * FROM publisher WHERE pub_id = (?)';
    var queryData = [publisher_id];
    console.log(query);

    connection.query(query, [queryData], function(err, result) {
        callback(err, result);
    });
};

exports.insert = function(params, callback) {

    // FIRST INSERT THE publisher
    var query = 'INSERT INTO publisher (pub_name, city, state_country) VALUES (?)';

    var queryData = [params.pub_name, params.city, params.state_country];

    connection.query(query,[queryData], function(err, result) {
        callback(err, result);
    });

};

exports.delete = function(publisher_id, callback) {
    var query = 'DELETE FROM publisher WHERE pub_id = ?';
    var queryData = [publisher_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });

};

exports.update = function(params, callback) {
    var query = 'UPDATE publisher SET pub_name = ?, city = ?, state_country = ? WHERE pub_id = ?';
    var queryData = [params.pub_name, params.city, params.state_country, params.pub_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};

exports.edit = function(publisher_id, callback) {
    var query = 'SELECT * FROM publisher WHERE pub_id = (?);';
    var queryData = [publisher_id];

    connection.query(query, queryData, function(err, result) {
        callback(err, result);
    });
};