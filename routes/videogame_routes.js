var express = require('express');
var router = express.Router();
var videogame_dal = require('../model/videogame_dal');
var publisher_dal = require('../model/publisher_dal');
var developer_dal = require('../model/developer_dal');

// View All videogame
router.get('/all', function(req, res) {
    videogame_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('videogame/videogameViewAll', { vg:result });
        }
    });

});

// View the videogame for the given id
router.get('/', function(req, res){
    if(req.query.vg_id == null) {
        res.send('vg_id is null');
    }
    else {
        videogame_dal.getById(req.query.vg_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('videogame/videogameViewById', {
                   vg: result[0],
                   dev: result[3],
                   pub: result[2]});
           }
        });
    }
});

// Return the add a new videogame form
router.get('/add', function(req, res){
    publisher_dal.getAll(function(err,pub) {
    if (err) {
        res.send(err);
    }
    else {
        developer_dal.getAll(function(err,dev) {
            if (err) {
                res.send(err);
            }
            else {
                res.render('videogame/videogameAdd', {dev: dev, pub: pub});
            }
        });
    }
    });
});

// View the videogame for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.vg_name == null) {
        res.send('Video Game name is required.');
    }
    else if(req.query.date_released == null) {
        res.send('Release date is required');
    }
    else if(req.query.rating == null) {
        res.send('Need a score');
    }
    else if(req.query.ESRB == null) {
        res.send('Needs an ESRB rating');
    }
    else if(req.query.platform == null) {
        res.send('Needs platform');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        videogame_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/videogame/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.vg_id == null) {
        res.send('A video game id is required');
    }
    else {
        publisher_dal.getAll(function(err,pub) {
        if (err) {
            res.send(err);
        }
        else {
            developer_dal.getAll(function(err,dev) {
                if (err) {
                    res.send(err);
                }
                else {
                    videogame_dal.edit(req.query.vg_id, function(err, result){
                        res.render('videogame/videogameUpdate', { vg: result[0],
                            full_pub: pub,
                            full_dev: dev,
                            dev: result,
                            pub: result});
                    });
                }
            });
        }
    });

    }

});

router.get('/update', function(req, res) {
    videogame_dal.update(req.query, function(err, result){
       res.redirect(302, '/videogame/all');
    });
});

// Delete a videogame for the given videogame_id
router.get('/delete', function(req, res){
    if(req.query.vg_id == null) {
        res.send('vg_id is null');
    }
    else {
         videogame_dal.delete(req.query.vg_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 //poor practice, but we will handle it differently once we start using Ajax
                 res.redirect(302, '/videogame/all');
             }
         });
    }
});

module.exports = router;
