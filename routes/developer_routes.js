var express = require('express');
var router = express.Router();
var developer_dal = require('../model/developer_dal');


// View All dev
router.get('/all', function(req, res) {
    developer_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('developer/developerViewAll', { dev: result });
        }
    });

});

// View the dev for the given id
router.get('/', function(req, res){
    if(req.query.dev_id == null) {
        res.send('dev_id is null');
    }
    else {
        developer_dal.getById(req.query.dev_id, function(err,result) {
           if (err) {
               res.send(err);
           }
           else {
               res.render('developer/developerViewById', {dev: result[0]});
           }
        });
    }
});

router.get('/add', function(req, res){
    res.render('developer/developerAdd');
});

// View the dev for the given id
router.get('/insert', function(req, res){
    // simple validation
    if(req.query.dev_name == null) {
        res.send('dev_name is required.');
    }
    else if(req.query.type == null) {
        res.send('Need to specify a type');
    }
    else if(req.query.status == null) {
        res.send('Need to specify status');
    }
    else if(req.query.city == null) {
        res.send('Developer must have a city');
    }
    else if(req.query.state_country == null) {
        res.send('Developer must have a state/country');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        developer_dal.insert(req.query, function(err,result) {
            if (err) {
                console.log(err)
                res.send(err);
            }
            else {
                //poor practice for redirecting the dev to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/developer/all');
            }
        });
    }
});

router.get('/edit', function(req, res){
    if(req.query.dev_id == null) {
        res.send('A dev id is required');
    }
    developer_dal.edit(req.query.dev_id, function(err, result){
        res.render('developer/developerUpdate', { dev: result[0]});
    });
});

router.get('/update', function(req, res) {
    developer_dal.update(req.query, function(err, result){
       res.redirect(302, '/developer/all');
    });
});

// Delete a dev for the given dev_id
router.get('/delete', function(req, res){
    if(req.query.dev_id == null) {
        res.send('dev_id is null');
    }
    else {
         developer_dal.delete(req.query.dev_id, function(err, result){
             if(err) {
                 res.send(err);
             }
             else {
                 res.redirect(302, '/developer/all');
             }
         });
    }
});

module.exports = router;
